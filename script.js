$('.tabs li').click(function () {
    $(this).addClass('active').siblings().removeClass('active');
    const tabContent = $(this).index();
    $('.tabs-content li').removeClass('active').eq(tabContent).addClass('active');
});